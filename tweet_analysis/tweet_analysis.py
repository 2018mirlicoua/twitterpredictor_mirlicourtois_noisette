import numpy as np
from twitter_collect.to_dataframe import *
from twitter_collect.tweet_collect import collect

tweets1 = collect('Macron')
data = transform_to_dataframe(tweets1)

rt_max  = np.max(data['RTs'])
rt  = data[data.RTs == rt_max].index[0]

# Max RTs:
print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
print("Number of retweets: {}".format(rt_max))
print("{} characters.\n".format(data['len'][rt]))


like_max = np.max(data['likes'])
like = data[data.likes == like_max].index[0]

# Max likes:
print("The tweet with more likes is: \n{}".format(data['tweet_textual_content'][like]))
print("Number of likes: {}".format(like_max))
print("{} characters.\n".format(data['len'][rt]))

lenght_max = np.max(data['len'])
lenght = data[data.len == lenght_max].index[0]

# Max lenght:
print("The longest tweet is: \n{}".format(data['tweet_textual_content'][lenght]))
print("lenght: {}".format(lenght_max))

