import matplotlib.pyplot as plt
from twitter_collect.to_dataframe import *
from twitter_collect.tweet_collect import collect

tweets = collect('Macron')
data = transform_to_dataframe(tweets)


tfav = pd.Series(data=data['likes'].values, index=data['Date'])
tret = pd.Series(data=data['RTs'].values, index=data['Date'])
tlenght = pd.Series(data=data['len'].values, index=data['Date'])
tnbr= pd.Series(len(data.index),index=data['Date'])

# Likes vs retweets visualization:
tfav.plot(figsize=(16,4), label="Likes", legend=True)
tret.plot(figsize=(16,4), label="Retweets", legend=True)


# retweets vs lenght visualization:
tret.plot(figsize=(16,4), label="Retweets", legend=True)
tlenght.plot(figsize=(16,4), label="lenght", legend=True)

# lenght vs lenght visualization:
tfav.plot(figsize=(16,4), label="Likes", legend=True)
tlenght.plot(figsize=(16,4), label="lenght", legend=True)

# likes vs number of tweets with word Macron visualization:
tfav.plot(figsize=(16,4), label="Likes", legend=True)
tnbr.plot(figsize=(16,4), label="number of tweets", legend=True)

plt.show()
