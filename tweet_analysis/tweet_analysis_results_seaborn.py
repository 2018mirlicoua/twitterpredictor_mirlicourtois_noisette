import seaborn as sns
import matplotlib.pyplot as plt
from twitter_collect.to_dataframe import *
from textblob import TextBlob

tweets = collect('Macron')
data = transform_to_dataframe(tweets)


# Number of tweets due to number of retweets
sns.countplot(x=data['RTs'], data=data)


# Number of tweets due to number of likes
sns.countplot(x=data['likes'], data=data)

#opinion of a tweet

def tweet_opinion1(tweet):
    if TextBlob(tweet.text).sentiment[0]>0:
        return 'positive tweet'
    elif TextBlob(tweet.text).sentiment[0]<0:
        return 'negative tweet'
    else:
        return 'neutral tweet'

#dataframe with textual content and opinion
def dict_opinion(tweets):
    dictlist = []
    for tweet in tweets:
        print(tweet)
        dictlist.append({"tweet_textual_content": str(tweet.text),
            "Opinion": tweet_opinion1(tweet)})
    return dictlist

data1=pd.DataFrame(dict_opinion(tweets))

# Opinion tweets
sns.countplot(x=data1['Opinion'],data=data1)


plt.xticks(rotation=-45)
plt.show()

