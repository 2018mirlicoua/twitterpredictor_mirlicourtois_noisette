from twitter_collect.to_dataframe import *
from textblob import TextBlob


def tweet_opinion(search_term):
    tweets = collect(search_term)
    data=transform_to_dataframe(tweets)
    positive_tweets = []
    negative_tweets = []
    neutral_tweets = []
    for tweet in tweets:
        if TextBlob(tweet.text).sentiment[0] < 0:   #on teste avec textblob l'opinion du tweets
            negative_tweets.append(tweet)
        elif TextBlob(tweet.text).sentiment[0] > 0:
            positive_tweets.append(tweet)
        else:
            neutral_tweets.append(tweet)
    # on transforme en dataframe puis on print
    pos_tweets = transform_to_dataframe(positive_tweets)
    neg_tweets = transform_to_dataframe(negative_tweets)
    neu_tweets = transform_to_dataframe(neutral_tweets)
    print("Percentage of positive tweets: {}%".format(len(pos_tweets) * 100 / len(data['tweet_textual_content'])))
    print("Percentage of neutral tweets: {}%".format(len(neu_tweets) * 100 / len(data['tweet_textual_content'])))
    print("Percentage de negative tweets: {}%".format(len(neg_tweets) * 100 / len(data['tweet_textual_content'])))


tweet_opinion('Macron')
