from textblob import TextBlob
from twitter_collect.tweet_collect import collect


def unique_lemmatize(search_term):
    text_tweet = collect(search_term)[0]
    text_tweet1 = TextBlob(text_tweet.text)
    words_list = text_tweet1.words
    words_lemmatized = [word.lemmatize() for word in words_list]
    return words_lemmatized

unique_lemmatize('Macron')



