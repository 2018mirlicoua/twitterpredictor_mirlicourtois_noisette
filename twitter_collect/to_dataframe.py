import pandas as pd
from twitter_collect.tweet_collect import collect
tweets1 = collect('Macron')

def transform_to_dict(tweet):
    return {"tweet_textual_content": str(tweet.text),
            "len" : len(tweet.text),
            "likes": tweet.favorite_count,
            "RTs": tweet.retweet_count,
            "Date": tweet.created_at}

def transform_to_dataframe(tweets):
    dict_list = []
    for tweet in tweets:
        dict_list.append(transform_to_dict(tweet))
    data = pd.DataFrame(dict_list)
    return data

print(transform_to_dataframe(tweets1))
