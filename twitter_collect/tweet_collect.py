from twitter_collect.twitter_connection_setup import twitter_setup
from tweepy.streaming import StreamListener
import tweepy


def collect(search_term):
    """
    The function takes a search term and returns the related tweets
    """
    print('Collecting the tweets related to {}'.format(search_term))
    connexion = twitter_setup()
    tweets = connexion.search(search_term, language="french", rpp=100)
    return tweets

tweets = collect('Macron')
for tweet in tweets:
    print(tweet.text)

class StdOutListener(StreamListener):
    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        if str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True

def collect_by_streaming(term_to_track):
    connexion = twitter_setup()
    listener = StdOutListener()
    stream = tweepy.Stream(auth=connexion.auth, listener=listener)
    stream.filter(track=[term_to_track])

def collect_by_user(user_id):
    """
    The function takes a user_id and returns its status
    """
    connexion = twitter_setup()
    username = connexion.get_user(user_id).screen_name
    print('Collecting the statuses of the username {} with ID {}'.format(username, user_id))
    statuses = connexion.user_timeline(id=user_id, count=200)
    for status in statuses:
        print(status.text)
    return statuses
